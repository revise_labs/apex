package com.reviselabs.apex.contracts;

import com.reviselabs.apex.routing.ApexRoutingContext;

/**
 * Author: Kevin Sheppard <kevin@reviselabs.com>
 * Created: 04/03/2017
 *
 * An interface for use in request handling. No duh.
 */
public interface RequestHandler {
  void handle(ApexRoutingContext context);
}