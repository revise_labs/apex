package com.reviselabs.apex.di

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Author: Kevin Sheppard <kevin@reviselabs.com>
 * Created: 04/03/2017
 *
 * The central point for all dependency injection.
 */
public class DependencyManager {
  private static Injector injector;
  private static Logger logger = LoggerFactory.getLogger(DependencyManager.class);

  /**
   * Initializes the injector with a {@link java.util.List} of
   * {@link com.google.inject.AbstractModule}.
   * @param modules
   * @return An initialized {@link Injector} ready for use.
   */
  public static Injector initializeWith(List<AbstractModule> modules) {
    if (injector == null) {
      logger.debug("Initializing injector");
      injector = Guice.createInjector(modules);
    }
    return getInjector();
  }

  /**
   * Basic getter for the {@link #injector }.
   * @return {@link #injector}
   */
  public static Injector getInjector() {
    if (injector == null) {
      logger.error("Application hasn't been configured yet.");
      return null;
    }
    return injector;
  }

  private DependencyManager() {}
}
