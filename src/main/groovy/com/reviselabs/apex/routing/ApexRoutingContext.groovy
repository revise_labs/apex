package com.reviselabs.apex.routing

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.inject.Inject
import com.google.inject.name.Named
import com.reviselabs.apex.config.Environment
import com.reviselabs.apex.contracts.ApplicationContextContainer
import com.reviselabs.apex.di.DependencyManager
import com.reviselabs.validation.Form
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.templ.TemplateEngine
import javax.annotation.Nullable
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Author: Kevin Sheppard <kevin@reviselabs.com>
 * Created: 04/03/2017
 *
 * A wrapper class for {@link RoutingContext}.
 * Provides a few nifty convenience methods.
 */
class ApexRoutingContext implements ApplicationContextContainer {

  // In IntelliJ this will cause problems with Java files, but only in IntelliJ.
  @Delegate
  RoutingContext context
  TemplateEngine templateEngine
  Logger logger
  String templatesPath

  @Inject
  ApexRoutingContext(@Nullable TemplateEngine templateEngine, @Named("templatesPath") String templatesPath) {
    this.templateEngine = templateEngine
    this.templatesPath = templatesPath
    logger = LoggerFactory.getLogger(getClass())
  }

  /**
   * Get the resqest body as a {@link Form} object for validation purposes.
   * Only works with JSON bodies right now.
   * @param type
   * @return
   */
  public <T> Form<T> getBodyAsForm(Class<T> type) {
    logger.warn("getBodyAsForm<T>: This only works for JSON body types at the moment. Will fix in subsequent release. Maybe.")
    try {
      //TODO: Cater for application/x-www-form-urlencoded
      def instance = new ObjectMapper().readValue(bodyAsString, type)
      return Form.form(type).bind(instance);
    } catch (JsonProcessingException e) {
      logger.error(e.message)
      return Form.form(type).bind(type.newInstance())
    }
  }

  @Override
  public <T> T getInstance(Class<T> clazz) {
    DependencyManager.injector.getInstance(clazz);
  }

  /**
   * Sets the response code to 200.
   * @return
   */
  ApexRoutingContext ok() {
    response().setStatusCode(HttpURLConnection.HTTP_OK);
    return this;
  }

  /**
   * Sets the response code to 400.
   * @return
   */
  ApexRoutingContext badRequest() {
    response().setStatusCode(HttpURLConnection.HTTP_BAD_REQUEST);
    return this;
  }

  /**
   * Sets the response code to 403.
   * @return
   */
  ApexRoutingContext forbidden() {
    response().setStatusCode(HttpURLConnection.HTTP_FORBIDDEN);
    return this;
  }

  /**
   * Sets the response code to 500.
   * @return
   */
  ApexRoutingContext error() {
    response().setStatusCode(HttpURLConnection.HTTP_INTERNAL_ERROR)
    return this;
  }

  /**
   * End the response with an optional {@link String}
   * @param message
   */
  void end(String message = '') {
    response().end(message);
  }

  /**
   * Redirect the user to another URL. Different from the {@link #reroute} method.
   * @param location
   */
  void redirect(String location) {
    response().setStatusCode(HttpURLConnection.HTTP_MOVED_TEMP).putHeader("location", location)
  }

  /**
   * Render a JSON response.
   * @param body A well-formed JSON string.
   */
  void renderJson(String body) {
    response()
        .putHeader("content-type", "application/json")
        .putHeader("content-length", String.valueOf(body.length()))
        .write(body).end();
  }

  /**
   * Render a JSON response.
   * @param bean A POJO that can be serialized by Jackson.
   */
  void renderJson(Object bean) {
    String json = new ObjectMapper().writeValueAsString(bean)
    renderJson(json)
  }

  /**
   * Render a plain text response.
   * @param text Text to be rendered.
   */
  void renderText(String text) {
    response()
        .putHeader("Content-Type", "text/plain")
        .putHeader("content-length", String.valueOf(text.length()))
        .write(text).end()
  }

  /**
   * Render a view template.
   * @param template The path to the template file.
   * @param data A {@link Map} of data to be passed to the template.
   */
  void render(String template, Map data = [:]) {
    data.forEach({ String k, Object v -> this.put(k, v) })
    if (templateEngine) {
      templateEngine.render(this, "$templatesPath/$template", { result ->
        def response = response().putHeader("content-type", "text/html");
        if (result.succeeded()) {
              response.putHeader("content-length", String.valueOf(result.result().length()))
              .write(result.result())
              .end();
        } else {
          error()
          StringBuilder message = new StringBuilder();
          message.append('<div style="font-family: sans-serif"> <h1>Rendering Error Occurred</h1>')
          message.append("<p>${result.cause().message}</p>")
          if (Environment.isDev()) {
            message.append('<ul style="list-style: none">')
            result.cause().stackTrace.each { trace ->
              message.append("<li>${trace}</li>")
            }
            message.append("</ul>")
          }
          logger.error(result.cause().message)
          message.append("</div>")
          response.putHeader("content-length", String.valueOf(message.toString().length()))
              .write(message.toString()).end()
          fail(HttpURLConnection.HTTP_INTERNAL_ERROR)
        }
      })
    } else {
      error().renderText("No template engine defined.")
      fail(HttpURLConnection.HTTP_INTERNAL_ERROR)
    };
  }
}
