package com.reviselabs.apex.routing;

import com.google.inject.Inject;
import com.reviselabs.apex.contracts.ApexModule;
import com.reviselabs.apex.contracts.ApplicationContextContainer;
import com.reviselabs.apex.di.DependencyManager;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;

/**
 * Author: Kevin Sheppard <kevin@reviselabs.com>
 * Created: 04/03/2017
 *
 * Base class used when the user needs to split up his routes.
 */
public abstract class SubRouter extends RoutingComponent implements ApplicationContextContainer, ApexModule {
  private Router parent;

  @Inject
  private void _setRouter(Vertx vertx) {
    assert vertx != null;
    setRouter(Router.router(vertx));
  }

  public Router getParent() {
    return parent;
  }

  @Override
  public <T> T getInstance(Class<T> clazz) {
    return DependencyManager.getInjector().getInstance(clazz);
  }

  public void setParent(Router parent) {
    this.parent = parent;
  }
}
