package com.reviselabs.apex.routing;

import com.reviselabs.apex.contracts.RequestHandler;
import com.reviselabs.apex.di.DependencyManager;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import java.util.Arrays;

/**
 * Author: Kevin Sheppard <kevin@reviselabs.com>
 * Created: 04/03/2017
 *
 * Base class that provides all the shortcut routing methods.
 */
public abstract class RoutingComponent {

  private Router router;

  public Router getRouter() {
    return router;
  }

  public void setRouter(Router router) {
    this.router = router;
  }

  /**
   * Internal method for creating the actual {@link io.vertx.ext.web.Route}.
   * @param method
   * @param url
   * @param handler
   * @return
   */
  private RoutingComponent doRoute(HttpMethod method, String url, RequestHandler handler) {
    router.route(method, url)
        .handler(context -> handler.handle(createContext(context)));
    return this;
  }

  /**
   * Takes a {@link RoutingContext} and converts it into an {@link ApexRoutingContext}.
   * @param context
   * @return
   */
  public ApexRoutingContext createContext(RoutingContext context) {
    @SuppressWarnings("ConstantConditions")
    ApexRoutingContext apexRoutingContext = DependencyManager.getInjector().getInstance(ApexRoutingContext.class);
    apexRoutingContext.setContext(context);
    return apexRoutingContext;
  }

  /**
   * Attach a single handler to a GET URL.
   * @param url
   * @param handler
   */
  //TODO: Resolve why these routing methods don't work in Java classes
  public void get(String url, RequestHandler handler) {
    doRoute(HttpMethod.GET, url, handler);
  }

  /**
   * Attach several handlers to a GET URL.
   * @param url
   * @param handlers
   */
  public void get(String url, RequestHandler... handlers) {
    Arrays.asList(handlers).forEach(handler -> get(url, handler));
  }

  /**
   * Attach a handler to POST URL.
   * @param url
   * @param handler
   */
  public void post(String url, RequestHandler handler) {
    doRoute(HttpMethod.POST, url, handler);
  }

  /**
   * Attach several handlers to a POST URL.
   * @param url
   * @param handlers
   */
  public void post(String url, RequestHandler... handlers) {
    Arrays.asList(handlers).forEach(handler -> post(url, handler));
  }

  /**
   * Attach a handler to a PUT URL.
   * @param url
   * @param handler
   */
  public void put(String url, RequestHandler handler) {
    doRoute(HttpMethod.PUT, url, handler);
  }

  /**
   * Attach several handlers to a PUT URL.
   * @param url
   * @param handlers
   */
  public void put(String url, RequestHandler... handlers) {
    Arrays.asList(handlers).forEach(handler -> put(url, handler));
  }

  /**
   * Attach a handler to a DELETE URL.
   * @param url
   * @param handler
   */
  public void delete(String url, RequestHandler handler) {
    doRoute(HttpMethod.DELETE, url, handler);
  }

  /**
   * Attach several handlers to a DELETE URL.
   * @param url
   * @param handlers
   */
  public void delete(String url, RequestHandler... handlers) {
    Arrays.asList(handlers).forEach(handler -> delete(url, handler));
  }

  /**
   * Add a Before Filter to a path.
   * @param url
   * @param handler
   */
  public void before(String url, RequestHandler handler) {
    router.route(url).handler(context -> handler.handle(createContext(context)));
  }

  /**
   * Add a Before Filter to all paths.
   * @param handler
   */
  public void before(RequestHandler handler) {
    router.route().handler(context -> handler.handle(createContext(context)));
  }

  /**
   * Turn a {@link RoutingContext} into a {@link ApexRoutingContext}.
   * @param context
   * @return
   */
  public ApexRoutingContext wrap(RoutingContext context) {
    return createContext(context);
  }
}
