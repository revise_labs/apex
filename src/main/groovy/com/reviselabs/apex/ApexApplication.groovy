package com.reviselabs.apex

import com.google.inject.AbstractModule
import com.reviselabs.apex.config.BaseConfiguration
import com.reviselabs.apex.config.Environment
import com.reviselabs.apex.contracts.ApexModule
import com.reviselabs.apex.contracts.ApplicationContextContainer
import com.reviselabs.apex.di.DependencyManager
import com.reviselabs.apex.routing.RoutingComponent
import com.reviselabs.apex.routing.SubRouter
import io.vertx.core.AsyncResult
import io.vertx.core.Handler
import io.vertx.core.Vertx
import io.vertx.core.VertxOptions
import io.vertx.core.http.HttpMethod
import io.vertx.core.http.HttpServer
import io.vertx.core.http.HttpServerOptions
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.CookieHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.ext.web.templ.TemplateEngine
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Author: Kevin Sheppard <kevin@reviselabs.com>
 * Created: 04/03/2017
 *
 * Base class for creating an Apex Application.
 *
 * <pre>{@code
 * class MyApp extends ApexApplication {
 *
 *   @Override
 *   void configure() {
 *     get('/', { ctx -> ctx.ok().renderText('Hello, world!') })
 *   }
 *
 *   static void main(String[] args) {
 *     new MyApp().start()
 *   }
 *
 * }
 * }</pre>
 */
abstract class ApexApplication extends RoutingComponent implements ApplicationContextContainer, ApexModule {
  protected Logger logger
  protected Vertx vertx
  protected HttpServer server;
  protected HttpServerOptions serverOptions
  protected TemplateEngine templateEngine
  protected String templatesPath
  private StaticHandler defaultStaticHandler
  private ArrayList<AbstractModule> modules
  private ArrayList<SubRoute> subRoutes

  ApexApplication(VertxOptions options = new VertxOptions()) {
    defaultStaticHandler = StaticHandler.create().setCachingEnabled(Environment.isProd())
    logger = LoggerFactory.getLogger(getClass())
    modules = new ArrayList<>()
    subRoutes = new ArrayList<>()
    vertx = Vertx.vertx(options)
    router = Router.router(vertx)
    templatesPath = 'public/templates'
    serverOptions = new HttpServerOptions(port: 3000)
  }

  /**
   * Generic method to add a handler for several {@link HttpMethod}s.
   * @param handler
   * @param methods
   */
  void addHandler(Handler<RoutingContext> handler, HttpMethod... methods) {
    if (methods) methods.each { router.route().method(it).handler(handler) }
    else router.route().handler(handler);
  }

  /**
   * Create a {@link StaticHandler} to serve static assets.
   * @param webRoot
   * @return
   */
  static StaticHandler createStaticHandler(String webRoot = "public") {
    StaticHandler handler = StaticHandler.create();
    handler.setWebRoot(webRoot).setCachingEnabled(Environment.isProd());
    return handler;
  }

  /**
   * Enable parsing of Cookies.
   */
  void enableCookies() {
    addHandler(CookieHandler.create());
  }

  /**
   * Enable parsing of request bodies.
   */
  void enableForms() {
    addHandler(BodyHandler.create(), HttpMethod.POST, HttpMethod.PUT, HttpMethod.PATCH);
  }

  @Override
  public <T> T getInstance(Class<T> clazz) {
    return DependencyManager.getInjector().getInstance(clazz);
  }

  Logger getLogger() {
    return logger
  }

  Vertx getVertx() { return vertx }

  /**
   * Mount a {@link SubRouter} to the application. Useful for splitting up
   * the routing table to create more maintainable code.
   * @param prefix
   * @param subRouterClass
   */
  void mount(String prefix, Class<? extends SubRouter> subRouterClass) {
    subRoutes.add(new SubRoute(prefix: prefix, _class: subRouterClass))
  }

  /**
   * Method that actually does all of the {@link SubRouter} mounting
   * once the {@link DependencyManager} has been initialized.
   */
  private void mountSubRoutes() {
    subRoutes.each { route ->
      SubRouter subRouter = getInstance(route._class)
      subRouter.parent = this.router
      subRouter.configure()
      router.mountSubRouter(route.prefix, subRouter.router)
    }
  }

  void register(AbstractModule module) {
    modules.add(module);
  }

  void setTemplatesPath(String templatesPath) {
    this.templatesPath = templatesPath
  }

  void staticFiles(String url, String webRoot, StaticHandler handler = defaultStaticHandler) {
    router.get(url).handler(handler.setWebRoot(webRoot))
  }

  void start(int port = serverOptions.port, Handler<AsyncResult> callback = {}) {
    logger.info("Running configuration...")
    configure()
    BaseConfiguration configuration = new BaseConfiguration(
        vertx: vertx,
        templateEngine:  templateEngine,
        templatesPath: templatesPath
    )
    modules.add(configuration)
    DependencyManager.initializeWith(modules)
    mountSubRoutes()
    logger.info("Configuration completed.")
    vertx.createHttpServer(serverOptions).requestHandler(router.&accept)
        .listen(port, { result ->
      if (result.succeeded()) {
        logger.info("Listening on port ${port}...");
        callback.handle(result);
      } else logger.error(result.cause().message);
    });
  }

  void start(Handler<AsyncResult> callback) {
    start(serverOptions.port, callback)
  }

  void stop(Handler<AsyncResult<Object>> callback = {}) {
    logger.info("App is shutting down...");
    vertx.close({ result ->
      if (result.succeeded()) {
        logger.info("Bye!")
        callback.handle(result)
      } else logger.error(result.cause().message)
    });
  }

  private static class SubRoute {
    String prefix
    Class<? extends SubRouter> _class
  }

}
