import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Author: Kevin Sheppard
 * Date: 03/04/2017
 *
 * Test cases to ensure basic routing functionality.
 */

@RunWith(VertxUnitRunner)
public class RoutingComponentTest extends AppTestSuite {

  @Test
  void initializationTest(TestContext context) {
    context.assertTrue(true)
  }

  @Test
  void getRequestTest(TestContext context) {
    client.get('/', { res ->
      context.assertEquals(200, res.statusCode())
      res.bodyHandler({ body ->
        context.assertTrue(body.toString().equals("OK"))
        resolve()
      })
    }).end()
  }

  @Test
  void postRequestTest(TestContext context) {
    promise(context)
    String params = [name: 'Kevin', age: 27].collect { it }.join('&')
    client.post('/', { res ->
      context.assertEquals(200, res.statusCode())
      res.bodyHandler({ body ->
        context.assertTrue(body.toString().equals(params))
        println params
        resolve()
      })
    }).putHeader("content-length", "${params.size()}").write(params).end()
  }

  @Test
  void putRequestTest(TestContext context) {
    promise(context)
    String params = [id: 10002, name: 'Kevin', email: 'kevin@mail.com'].collect { it }.join('&')
    client.put('/', { res ->
      context.assertEquals(200, res.statusCode())
      res.bodyHandler({ body ->
        context.assertTrue(body.toString().equals(params))
        println params
        resolve()
      })
    }).putHeader("content-length", "${params.size()}").write(params).end()
  }

  @Test
  void deleteRequestTest(TestContext context) {
    promise(context)
    client.delete('/', { res ->
      context.assertEquals(200, res.statusCode())
      res.bodyHandler({ body ->
        context.assertTrue(body.toString().equals('DELETED'))
        resolve()
      })
    }).end()
  }

  @Test
  void renderTemplateTest(TestContext context) {
    promise(context)
    client.get('/greeting', { res ->
      context.assertEquals(200, res.statusCode())
      res.bodyHandler({body ->
        context.assertTrue(body.toString().contains("Hello, world!"))
        resolve()
      })
    }).end()
  }

  @Test
  void staticFilesTest(TestContext context) {
    promise(context)
    client.get('/assets/')
  }
}
