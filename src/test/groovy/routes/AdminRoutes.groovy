package routes

import com.reviselabs.apex.routing.SubRouter

/**
 * Created by Kevin on 3/4/2017.
 *
 * An example of how to create a SubRouter
 */
class AdminRoutes extends SubRouter {

  @Override
  void configure() {
    get('/', {ctx ->
      ctx.ok().renderText("/admin");
    })

    get('/me', {ctx ->
      ctx.ok().renderText(ctx.get('admin').toString())
    })
  }
}
