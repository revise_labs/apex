import com.reviselabs.apex.ApexApplication
import io.vertx.core.http.HttpClient
import io.vertx.core.http.HttpClientOptions
import io.vertx.ext.unit.Async
import io.vertx.ext.unit.TestContext
import org.junit.AfterClass
import org.junit.BeforeClass
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Author: Kevin Sheppard <kevin@reviselabs.com>
 * Created: 03/04/2017
 *
 * Common methods and properties for the test cases.
 */
class AppTestSuite {

  @BeforeClass
  static void setUp(TestContext context) {
    logger = LoggerFactory.getLogger(getClass())
    startServer(context)
  }

  @AfterClass
  static void tearDown() {
    app.stop()
  }

  static ApexApplication app
  static Async async
  static HttpClient client
  static Logger logger

  static void promise(TestContext context) {
    async = context.async();
  }

  static void resolve() {
    async.complete();
  }

  static void startServer(TestContext context) {
    promise(context)
    app = new ExampleApplication()
    app.start(3000, { resolve() });
    client = app.vertx.createHttpClient(new HttpClientOptions(defaultHost: 'localhost', defaultPort: 3000));
  }
}
