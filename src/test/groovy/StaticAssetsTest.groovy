import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Kevin Sheppard on 3/4/2017.
 *
 * Tests to ensure that that router can serve basic web files (Html, CSS, JavaScript)
 */
@RunWith(VertxUnitRunner)
class StaticAssetsTest extends AppTestSuite {

  @Test
  void serveJavaScriptFileTest(TestContext context) {
    promise(context)
    client.get("/assets/app.js", { response ->
      context.assertEquals(200, response.statusCode())
      context.assertEquals(response.getHeader("content-type"), 'application/javascript')
      response.bodyHandler({ body -> context.assertTrue(body.toString().contains("function")); resolve(); })
    }).end()
  }

  @Test
  void serveHtmlFileTest(TestContext context) {
    promise(context)
    client.get("/assets/index.html", { response ->
      context.assertEquals(200, response.statusCode())
      context.assertEquals(response.getHeader("content-type"), 'text/html;charset=UTF-8')
      response.bodyHandler({ body -> context.assertTrue(body.toString().contains("<body>")); resolve(); })
    }).end()
  }

  @Test
  void serveCssFileTest(TestContext context) {
    promise(context)
    client.get("/assets/style.css", { response ->
      context.assertEquals(200, response.statusCode())
      context.assertEquals(response.getHeader("content-type"), 'text/css;charset=UTF-8')
      response.bodyHandler({ body -> context.assertTrue(body.toString().contains("body")); resolve(); })
    }).end()
  }

}
