package models

import com.reviselabs.validation.constraints.Email
import com.reviselabs.validation.constraints.Required
import groovy.transform.ToString

/**
 * Created by Kevin on 3/4/2017.
 *
 * Model class for testing form validation
 */
@ToString
class User {
  @Required
  String name
  @Required @Email
  String email
}
