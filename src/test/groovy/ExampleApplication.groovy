import com.reviselabs.apex.ApexApplication
import com.reviselabs.validation.Form
import config.TestConfig
import data.DAO
import io.vertx.ext.web.templ.JadeTemplateEngine
import models.User
import routes.AdminRoutes

public class ExampleApplication extends ApexApplication {

  @Override
  void configure() {

    templateEngine = JadeTemplateEngine.create().setExtension('.jade')

    templatesPath = 'src/test/files/templates'

    enableForms()

    staticFiles('/assets/*', 'src/test/files')

    register(new TestConfig())

    get('/', { ctx -> ctx.ok().end('OK') })

    post("/", { ctx ->
      logger.debug("In a callback")
      ctx.ok().end(ctx.body.toString())
    });

    put("/", { ctx ->
      ctx.ok().end(ctx.body.toString())
    });

    delete("/", { ctx -> ctx.ok().end('DELETED') });

    get('/greeting', { ctx ->
      ctx.put("name", "Vert.x Web");
      ctx.render("greeting", [ greeting: 'Hello, world!' ]);
    })

    before('/admin/me', { ctx ->
      ctx.put('admin', true)
      ctx.next()
    })

    before({ it.put('user', 'kevin@mail.com'); it.next() })

    mount('/admin', AdminRoutes)

    get('/db', { ctx ->
      String db = ctx.getInstance(DAO).database.toString()
      ctx.ok().renderText(db)
    })

    get('/me', {ctx ->
      ctx.ok().renderText(ctx.get('user').toString())
    })

    post('/users', {ctx ->
      Form<User> userForm = ctx.getBodyAsForm(User)
      if(userForm.valid())
        ctx.ok().renderText(userForm.get().toString())
      else
        ctx.badRequest().renderJson(userForm.errorsAsJson)
    })

  }

  static void main(String[] args) {
    new ExampleApplication().start()
  }
}
